import asyncio

import client_core.common as common


@asyncio.coroutine
def notify_all(message):
    print(message)
    yield from common.client.send_message(common.notify_id, message)
    yield from common.client.send_message(common.charwars_id, message)
