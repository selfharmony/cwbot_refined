items = {
    "Thread": {"buy_code": "/wtb_01", "craft_code": "/a_01", "av_price": 1},
    "Leather": {"buy_code": "/wtb_20", "craft_code": "/a_20", "av_price": 20},

}
recipes = {
}
# todo: классы вместо словарей чтобы произвдить вычисления
# todo: сохранение прогресса закупок в файл
# todo: в ui потом должен быть конструктор закупок
resources = {
    "pouch":
        {"must_craft": True,
         "to_craft": 99,
         "sum": 120,
         "mat_cost": 56,
         "craft": "/craft_100",
         "materials": [
             {"name": "Thread", "code": "/wtb_01", "amount": 12, "av_price": 26,
              "can_buy": True},
             {"name": "Leather", "code": "/wtb_20", "amount": 2, "av_price": 21,
              "can_buy": True}
         ]},
    "club":
        {"must_craft": False,
         "to_craft": 3,
         "sum": 62,
         "craft": "/buy_w23",
         },
    "skeleton buckler":
        {"must_craft": False,
         "to_craft": 5,
         "sum": 50,
         "craft": "/buy_a22",
         },
    "Leather shirt":
        {"must_craft": False,
         "to_craft": 0,
         "sum": 30,
         "craft": "/buy_a02",
         },
    "Leather shoes":
        {"must_craft": False,
         "to_craft": 4,
         "sum": 43,
         "craft": "/buy_a12",
         },
    "powder":
        {"must_craft": False,
         "to_craft": 0,
         "sum": 20,
         "craft": "/wtb_07_10",
         },
    "HunterArmor":
        {"must_craft": False,
         "to_craft": 1,
         "sum": 0,
         "mat_cost": 999999,
         "craft": None,
         "materials": [
             {"name": "Coke", "code": "/wtb_01_16", "amount": 9, "av_price": 14,
              "can_buy": True},
             {"name": "Leather", "code": "/wtb_20_2", "amount": 24, "av_price": 21,
              "can_buy": True},
             {"name": "Rope", "code": "/home/%(user)s", "amount": 12, "av_price": 14,
              "can_buy": True},
             {"name": "Sapphire", "code": "/wtb_01_16", "amount": 3, "av_price": 1,
              "can_buy": True},
             {"name": "Silver mold", "code": None, "amount": 3, "av_price": None,
              "can_buy": False},
             {"name": "Solvent", "code": "/wtb_01_16", "amount": 5, "av_price": 1,
              "can_buy": True},
             {"name": "Hunter Armor part", "code": None, "amount": 3, "av_price": None,
              "can_buy": False},
             {"name": "Hunter Armor recipe", "code": None, "amount": 1, "av_price": None,
              "can_buy": False}
         ]}
}