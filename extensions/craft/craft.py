import asyncio, re, time
import client_core.common as common
import extensions.notify as notify
from extensions.craft.resouces import resources
from client_core.common import scheduler

local_money = 0


def find_between(string, start, end):
    result = re.search(start + '(\d*)' + end, string)
    return result.group(1)


def get_quantity(what: str, where: str):
    result = re.search(what + '.+[^0-9.](\d+)', where)
    if result is None:
        return 0
    else:
        return int(result.group(1))


@asyncio.coroutine
def buy_one_res(res_command):
    yield from common.client.send_message(common.charwars_username, res_command)


@asyncio.coroutine
def buy_resources(msg, res):
    global local_money
    for recipe in list(res.values()):
        if recipe["must_craft"]:
            if recipe['materials'] is not None:
                for m in recipe['materials']:
                    if (not m['name'] in msg) | (get_quantity(m['name'], msg) < m['amount']):
                        if m['av_price'] <= local_money:
                            scheduler.add_
                            yield from asyncio.sleep(3.0)
                            yield from buy_one_res(m['code'])
                            local_money -= m['av_price']
                            yield from asyncio.sleep(3.0)
                            yield from buy_resources(msg, res)


# флаг накопления необходимой суммы для крафта того или иного ресурса
@asyncio.coroutine
def craft_items(msg, f_id):
    global local_money
    # importlib.reload(game_data)
    proceed = False
    for key, value in resources.items():
        if (resources[key]["must_craft"]) & (resources[key]["to_craft"] > 0):
            enough_money = local_money >= resources[key]["sum"]
            if enough_money:
                yield from asyncio.sleep(3.0)
                yield from common.client.send_message(f_id, resources[key]["craft"])
                local_money -= resources[key]["sum"]
                resources[key]["to_craft"] -= 1
                yield from asyncio.sleep(3.0)
                proceed = (enough_money | proceed)
    if proceed:
        yield from craft_items(msg, f_id)


def handle(event):
    global local_money, event_data
    msg = event.message.message
    f_id = event.message.from_id

    if event.message.from_id == common.charwars_id:
        print(time.asctime(), '-', msg)
        if ("💰" in msg) & ("👝" in msg):
            local_money = int(find_between(msg, '💰', ' 👝'))

            yield from craft_items(msg, f_id)
        if "📦Склад" in msg:
            yield from buy_resources(msg, resources)  # !!!!!!!!
        if "Получено:" in msg:
            common.scheduler.add_job(common.client.forward_messages, args=[common.stat_username, event.message])
            common.scheduler.add_job(common.get_hero_info)
            common.get_hero_info()
