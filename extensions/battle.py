import asyncio
import client_core.common as common

import extensions.notify as notify
import extensions.quests as quests

protec_m = "🛡Защита"

battles = [0, 8, 16]
is_battle = False

my_castle = '🍆'
enemy_castle = "🌹"
pin_insider_cache = []
enemy_is_pinned = False
lil_pin_id = 1293422180
lil_pin_name = "ChatWarslivpin"
pins_dict = {
    "🍆": ["🍆"],
    "🦇": ["🦇"],
    "☘️": ["☘️", "☘"],
    "🍁": ["🍁"],
    "🌹": ["🌹"],
    "🖤": ["🖤"],
    "🐢": ["🐢"]
}


# reply

def _get_pin(message):
    global enemy_castle
    if enemy_castle in message:
        return enemy_castle
    for key in pins_dict:
        if my_castle in key:
            continue
        for phrase in pins_dict[key]:
            if phrase.lower() in message.lower():
                return key
    return None


def handle(event):
    msg = event.message.message
    global enemy_castle, enemy_is_pinned
    if event.message.from_id == common.charwars_id:
        yield from asyncio.sleep(1.0)
        if "Твои результаты в бою:" in msg:
            yield from common.client.forward_messages(common.stat_username, event.message)
        yield from asyncio.sleep(2.0)
    elif event.message.to_id.channel_id == lil_pin_id:
        pin = _get_pin(event.message.message)
        if pin is not None:
            yield from notify.notify_all("message from lilpin: " + pin)
            pin_insider_cache.append(pin)
            if enemy_castle in pin:
                enemy_is_pinned = True


@asyncio.coroutine
def pin_castle(pin: str):
    pin_insider_cache.append(pin)
    yield from notify.notify_all("Добавлен пин на: " + pin)
    yield from notify.notify_all("Полученные пины до битвы: " + str(pin_insider_cache))


# plan

@asyncio.coroutine
def _defend():
    global protec_m
    common.scheduler.remove_all_jobs(jobstore="quests")
    common.scheduler.reschedule_job(quests._forest, 'interval', minutes=60, id="forest", jobstore="quests")
    yield from common.client.send_message(common.charwars_username, protec_m)


@asyncio.coroutine
def _atak():
    global enemy_is_pinned
    if enemy_is_pinned:
        yield from common.client.send_message(common.charwars_username, enemy_castle)
    elif len(pin_insider_cache) > 0:
        yield from common.client.send_message(common.charwars_username, pin_insider_cache[-1])
    yield from notify.notify_all(pin_insider_cache)
    pin_insider_cache.clear()
    enemy_is_pinned = False


@asyncio.coroutine
def _report():
    yield from common.client.send_message(common.charwars_username, "/report")


def plan():
    common.scheduler.add_job(_defend, 'cron', hour=0, minute=45, jobstore="battle")
    common.scheduler.add_job(_defend, 'cron', hour=8, minute=45)
    common.scheduler.add_job(_defend, 'cron', hour=16, minute=45)

    common.scheduler.add_job(_atak, 'cron', hour=0, minute=59, second=20, jobstore="battle")
    common.scheduler.add_job(_atak, 'cron', hour=8, minute=59, second=20, jobstore="battle")
    common.scheduler.add_job(_atak, 'cron', hour=16, minute=59, second=20, jobstore="battle")

    common.scheduler.add_job(_report, 'cron', hour=1, minute=11, jobstore="battle")
    common.scheduler.add_job(_report, 'cron', hour=9, minute=11, jobstore="battle")
    common.scheduler.add_job(_report, 'cron', hour=17, minute=11, jobstore="battle")
