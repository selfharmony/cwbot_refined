import asyncio, re, time
import random
import client_core.common as common
import extensions.notify as notify
from datetime import datetime

k_hour = 4
stamina = {'now': 5, 'total': 5}
local_time = {'h': 0, 'm': 0}

go_korovan_m = "🗡ГРАБИТЬ КОРОВАНЫ"
get_time_m = "/time"
quest_m = ["🌲Лес"]




def handle(event):
    global local_money, event_data
    msg = event.message.message
    print(time.asctime(), '-', msg)
    f_id = event.message.from_id

    korovans = ["КОРОВАН твой", "КОРОВАН уехал", "пришлось откупиться"]

    if "/go" in msg:
        common.scheduler.add_job(common.client.send_message, args=[f_id, "/go"])
    if "/engage" in msg:
        common.scheduler.add_job(common.client.send_message, args=[f_id, "/engage"])
    if any(k in msg for k in korovans):
        asyncio.sleep(1)
        common.get_hero_info()
        asyncio.sleep(5)
        common.scheduler.add_job(common.client.send_message,
                                 args=[common.charwars_username, go_korovan_m])
    if "Слишком мало единиц выносливости" in msg:
        common.get_hero_info()
    if "🔋Выносливость:" in msg:
        re_stamina = re.search('Выносливость: (\d+)/(\d+)', msg)
        stamina['now'] = int(re_stamina.group(1))
        stamina['total'] = int(re_stamina.group(2))


def _get_korovan_time_remaining():
    time = datetime.today()
    hour = time.hour
    minutes = 60 - time.minute
    if minutes > 9:
        min_str = str(minutes)
    elif minutes == 60:
        hour += 1
        minutes = 0
        min_str = str(minutes)
    else:
        min_str = '0' + str(minutes)
    if hour < k_hour:
        hour_str = str(k_hour - hour - 1)
    else:
        hour_str = str(24 - hour + k_hour - 1)

    time_str = hour_str + ':' + min_str
    message = 'До корованов осталось примерно: ' + time_str
    return message


any_forest_counter = 0


def _roll_quest():
    global quest_m
    return random.choice(quest_m)

@asyncio.coroutine
def _forest():
    global any_forest_counter
    yield from notify.notify_all('Идем в ЛЕС#' + str(any_forest_counter))
    yield from common.client.send_message(common.charwars_username, _roll_quest())
    any_forest_counter += 1


@asyncio.coroutine
def _korovan():
    global go_korovan_m
    yield from notify.notify_all('FUCKING KOROVANS')
    yield from common.client.send_message(common.charwars_username, go_korovan_m)
    common.scheduler.add_job(_forest, 'interval', minutes=60, id="forest", jobstore="quests")


@asyncio.coroutine
def _check_stamina():
    global stamina, k_hour
    hours_str = re.search('(\d+):', _get_korovan_time_remaining()).group(1)
    hours_before = int(hours_str)
    to_restore = stamina['total'] - stamina['now']
    if (hours_before > to_restore) & (datetime.today().hour >= k_hour + 1):
        msg = "До корованов: {k} ч. \nВыносливости: {s}".format(k=hours_before, s=stamina['now'])
        yield from notify.notify_all(msg)
    else:
        msg = "А теперь бережем выносливость. \nДо корованов: {k} ч. \nВыносливости: {s}".format(
            k=hours_before,
            s=stamina['now'])
        common.scheduler.remove_all_jobs(jobstore="quests")
        yield from notify.notify_all(msg)


def schedule_forest():
    global any_forest_counter
    any_forest_counter = 0
    common.scheduler.remove_all_jobs(jobstore="quests")
    common.scheduler.add_job(_forest, 'interval', minutes=60, id="forest", jobstore="quests")
    common.scheduler.get_job("forest", jobstore="quests").modify(next_run_time=datetime.now())
    print('=================')
    print('FOREST STARTED!')


def schedule_f_forest():
    global any_forest_counter
    any_forest_counter = 0
    common.scheduler.remove_all_jobs(jobstore="quests")
    common.scheduler.add_job(_forest, 'interval', minutes=8, id="f_forest", jobstore="quests")
    common.scheduler.get_job("f_forest", jobstore="quests").modify(next_run_time=datetime.now())
    print('=================')
    print('FAST FOREST STARTED!')


def schedule_korovan():
    common.scheduler.remove_all_jobs(jobstore="quests")
    common.scheduler.add_job(_korovan, 'cron', hour=4, id="korovan", jobstore="quests")
    print('=================')
    print('KOROVAN SCHEDULED!')


def schedule_ka4():
    common.scheduler.remove_all_jobs(jobstore="quests")
    schedule_forest()
    schedule_korovan()
    common.scheduler.add_job(_check_stamina, 'interval', minutes=45, id="c_stamina",
                             jobstore="quests")
    print('=================')
    print('KA4 STARTED!')
