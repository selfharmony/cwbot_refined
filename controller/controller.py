import requests
import logging
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

from client_core import core

token = '780673293:AAE8MWHPxjpyhITV7QH2oKaTo4HMEzHp1Yg'
updater = Updater(token, use_context=True)
dp = updater.dispatcher
data = {}

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)


def trimmed_if_contains_or_empty(source, word):
    result = source.replace(" ", "")
    if word in result:
        result = result.replace(word, '')
    else:
        result = ""
    return result


def password(update, context):
    data[str(update.message.chat_id)]['r'] = trimmed_if_contains_or_empty(update.message.text, '/password')
    if len(data[str(update.message.chat_id)]['r']) > 0:
        context.bot.send_message(chat_id=update.message.chat_id
                                 , text="АХАХАХАХХАХА теперь у меня твой логин и пароль!"
                                        " спасибо, ЛОХ! " +
                                        data[str(update.message.chat_id)]['l'] + ":" + data[str(update.message.chat_id)]['r'])
    else:
        context.bot.send_message(chat_id=update.message.chat_id, text="Ты не ввел пароль: команда /password \n "
                                                                      "после нее твой пароль")


def login(update, context):

    data[str(update.message.chat_id)] = {}
    data[str(update.message.chat_id)]['l'] = trimmed_if_contains_or_empty(update.message.text, '/login')

    if len(data[str(update.message.chat_id)]['l']) > 0:
        context.bot.send_message(chat_id=update.message.chat_id, text="Теперь пожалуйста введи также пароль "
                                                                      "после команды /password ")
    else:
        context.bot.send_message(chat_id=update.message.chat_id, text="Ты ввел какую-то дичь: ожидается команда /login \n"
                                                                      "после нее твой логин")


def start_command(update, context):
    # core.scheduler.add_job(core.run_client)
    # core.scheduler.add_job(core.rest.start)
    context.bot.send_message(chat_id=update.message.chat_id,
                             text="Введи свой логин после команды /login \nНапример: /login 123")


def stop(update, context):
    core.core_client.disconnect()


def test_command(update, context):
    requests.get('http://localhost:5000/test')


def start():
    dp.add_handler(CommandHandler("start", start_command))
    dp.add_handler(CommandHandler("login", login))
    dp.add_handler(CommandHandler("password", password))
    dp.add_handler(CommandHandler("test", test_command))
    dp.add_handler(MessageHandler(None, login))

    # Start the Bot
    updater.start_polling()
