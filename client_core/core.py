import asyncio


from telethon import events
from client_core.common import client as core_client
from client_core.common import scheduler
from client_core.common import loop
import client_core.rest_server as rest
import extensions.battle as battle
import extensions.craft.craft as craft
import extensions.quests as quests
from controller import controller
from extensions.notify import notify_all

modules = [battle, craft, quests]


def handle_with_modules(event):
    for m in modules:
        m.handle(event)


@core_client.on(events.NewMessage)
@asyncio.coroutine
def core_handler(event: events.NewMessage.Event):
    handle_with_modules(event)


@asyncio.coroutine
def run_client():
    print('стартуем клиент...')
    yield from core_client.start()
    print('=================')
    print("BOT STARTED")
    print('=================\n')
    yield from core_client.run_until_disconnected()


@asyncio.coroutine
def plan_actions():
    for m in modules:
        yield from m.plan()


@asyncio.coroutine
def hey(m):
    print(m)


def main():
    scheduler.start()
    scheduler.add_job(hey, args=["scheduler works"])
    # scheduler.add_job(run_client)
    # scheduler.add_job(rest.start)
    scheduler.add_job(controller.start)
    loop.run_forever()  # asyncio.gather(*tasks)


if __name__ == '__main__':
    try:
        main()
    # except KeyboardInterrupt:
    #     print("KeyboardInterrupt")
    #     exit_flg = True
    # except RuntimeError:
    #     print('RuntimeError!')
    # except Exception:
    #     print(Exception)
    finally:
        loop.close()
