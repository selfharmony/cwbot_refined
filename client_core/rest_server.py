import asyncio

from flask import Flask
from flask_restful import Resource, Api
from client_core.common import scheduler
from extensions.notify import notify_all

app = Flask(__name__)


@app.route("/test")
def test():
    print("test success")
    scheduler.add_job(notify_all, args=["passed"])
    return "Everybody notified!"


@app.route("/command/<command_id>")
def listen_command(command_id):
    scheduler.add_job(notify_all, args=[command_id])
    return "command success"


def start():
    print("rest listener started")
    app.run(host='127.0.0.1', port=5000)  # , debug=False, use_reloader=False
