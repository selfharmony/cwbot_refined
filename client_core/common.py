from datetime import datetime

import socks
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from telethon import TelegramClient, connection
from os import environ
import asyncio, re, time

scheduler = AsyncIOScheduler()
__api_id = 309116
__api_hash = '3e0ef95df207dde3ab60b722d198502a'

notify_id = '@SuperBoltunBot'
charwars_username = '@ChatWarsBot'
charwars_id = 265204902
stat_username = "@CWCastleBot"

session_name = environ.get('TG_SESSION', 'session')
loop = asyncio.get_event_loop()


host = 'russia.proxytelegram.com'
port = 8080
secret = 'ddfb175d6d7f820cdc73ab11edbdcdbd74'
proxy = (host, port, secret)

client = TelegramClient(
    session_name, __api_id, __api_hash,
    proxy=proxy,
    connection=connection.tcpmtproxy.ConnectionTcpMTProxyRandomizedIntermediate
)


def get_hero_info():
    scheduler.add_job(_hero_info_task)


def seconds_delay(delay):
    return datetime.now().second + delay


@asyncio.coroutine
def _hero_info_task():
    yield from client.send_message(charwars_username, "/stock")
    yield from asyncio.sleep(3.0)
    yield from client.send_message(charwars_username, "/hero")
    yield from asyncio.sleep(3.0)
    # yield from common.client.send_message(common.charwars_username, "/myshop_open")
    # yield from asyncio.sleep(3.0)


def stop_tasks(*tasks):
    for t in tasks:
        if scheduler.get_job(t) is not None:
            scheduler.remove_job(t)
